import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom'
import Layout from './components/Layout';

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Route path="/" component={Layout} />
      </BrowserRouter>
    );
  }
};
