import { createSelector } from 'reselect';
import { isLoaded, isEmpty } from 'react-redux-firebase';

const getProducts = state => state.firebase.profile.products;
const getObserves = state => state.firebase.profile.observes;
const getFridge = state => state.firebase.profile.fridge;
const isReady = state => isLoaded(state.firebase.profile) && !isEmpty(state.firebase.profile);

const notifications = (filter, product, fridge, isReadyState) => {
  if (!isReadyState) return [];

  const productsNotifications = product.reduce((acc, item) => {
    const includes = filter.findIndex((i) => {
      if (i.product === 'Любые') {
        return i.categories === item.categories;
      }
      return i.categories === item.categories && i.product === item.product;
    });

    if (includes !== -1) {
      const now = new Date(Date.now()).getDate();
      const dateBuing = new Date(item.date).getDate();

      if (dateBuing - now < 2) {
        return [...acc, { ...item, type: 'purchase' }];
      }
    }

    return acc;
  }, []);

  const fridgeNotifications = fridge.reduce((acc, item) => {
    const includes = filter.findIndex((i) => {
      if (i.product === 'Любые') {
        return i.categories === item.categories;
      }
      return i.categories === item.categories && i.product === item.product;
    });

    const newValues = [];

    if (includes !== -1) {
      if (item.amount === '1') {
        newValues.push({ ...item, type: 'amount' });
      }

      const now = new Date(Date.now()).getDate();
      const expiration = new Date(item.dateExpiration).getDate();

      if (now - expiration < 3) {
        newValues.push({ ...item, type: 'shelfLife' });
      }
    }

    return [...acc, ...newValues];
  }, []);

  return [...productsNotifications, ...fridgeNotifications];
};

const countNotifications = createSelector(
  getObserves,
  getProducts,
  getFridge,
  isReady,
  notifications,
);

export default countNotifications;
