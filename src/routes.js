// @material-ui/icons
import Dashboard from '@material-ui/icons/Dashboard';
import Person from '@material-ui/icons/Person';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import Notifications from '@material-ui/icons/Notifications';

import DashboardComponent from './pages/Dashboard';
import NotificationsComponent from './pages/Notifications';
import ProductsComponent from './pages/Products';
import ShoppingListComponent from './pages/ShoppingList';
import UserComponent from './pages/User';

const dashboardRoutes = [
  {
    path: '/dashboard',
    name: 'Главная',
    icon: Dashboard,
    component: DashboardComponent,
  },
  {
    path: '/list',
    name: 'Список покупок',
    icon: 'content_paste',
    component: ShoppingListComponent,
  },
  {
    path: '/products',
    name: 'Мои продукты',
    icon: LibraryBooks,
    component: ProductsComponent,
  },
  {
    path: '/notifications',
    name: 'Уведомления',
    icon: Notifications,
    component: NotificationsComponent,

  },
  {
    path: '/user',
    name: 'Избранное',
    icon: Person,
    component: UserComponent,
  },
];

export default dashboardRoutes;
