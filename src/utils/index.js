const getStrDate = (date) => {
  const month = date.getMonth() < 9 ? `0${date.getMonth() + 1}` : date.getMonth() + 1;

  const day = date.getDate();

  return `${day}.${month}`;
};

export default getStrDate;
