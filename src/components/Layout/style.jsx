import { drawerWidth, transition } from "../../assets/styles/material-dashboard-react.jsx";

const style = theme => ({
  wrapper: {
    position: "relative",
    top: "0",
    width: '100vw',
    height: "100vh",
  },
  mainPanel: {
    width: `calc(100% - ${drawerWidth}px)`,
    overflow: "auto",
    position: "relative",
    float: "right",
    ...transition,
    height: '100%',
    overflowScrolling: "touch",
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
    textAlign: '-webkit-center'
  },
  content: {
    maxWidth: '1921px',
    marginTop: "90px",
    padding: "30px 15px",
    minHeight: "calc(100vh - 170px)",
    flexGrow: 1,
    ...transition
  },
});

export default style;
