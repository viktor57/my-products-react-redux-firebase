import React from "react";
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from "react-router-dom";
import { compose } from 'redux';
import { firestoreConnect } from 'react-redux-firebase';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// Core components
import SideBar from '../SideBar';
import NavBar from '../NavBar';
import SignForm from '../SignForm';
import Dashboard from '../../pages/Dashboard';
//Styles
import layoutStyle from './style';
import routes from '../../routes';

const mapStateToProps = (state) => ({
  auth: state.firebase.auth
});

export default @connect(mapStateToProps)
@withStyles(layoutStyle)
class App extends React.Component {
  render() {
    const { classes, auth } = this.props;

    return (
      <div className={classes.wrapper}>
        <SideBar routes={routes} />
        <div className={classes.mainPanel} id='mainPanel'>
          <NavBar />
          <div className={classes.content}>
            <Switch>
                {routes.map((prop, key) => (
                  <Route
                    path={prop.path}
                    component={prop.component}
                    key={key}
                  />
                ))}
                <Route path='/signin' render={() => <SignForm type='signIn' />} />
                <Route path='/signup' render={() => <SignForm type='signUp' />} />
                {Boolean(auth.uid) ? <Redirect to='/dashboard' /> : <Redirect to='/signin' />}
            </Switch>
          </div>
        </div>
      </div>
    );
  }
};
