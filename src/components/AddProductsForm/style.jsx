const CardStyle = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    textAlign: 'auto'
  },
  formControl: {
    marginRight: '25px'
  },
  button: {
    margin: theme.spacing.unit,
    minWidth: '110px'
  },
  sendIcon: {
    marginLeft: theme.spacing.unit,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
});

export default CardStyle;
