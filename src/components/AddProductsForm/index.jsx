import React from "react";
import { connect } from 'react-redux';
import { addProduct } from '../../store/actions/productActions';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Send from '@material-ui/icons/Send';
// Core components
import DatePicker from '../DatePicker';
import Modal from '../Modal';
import SelectField from '../SelectField';
import SnackError from '../SnackError';
// data
import date from '../../dataStorage/date';
import amount from '../../dataStorage/amount';
import categories from '../../dataStorage/categories';
import products from '../../dataStorage/products';
//Styles
import MainStyle from './style';

const mapDispatchToProps = (dispatch) => {
  return {
    addProduct: (creds) => dispatch(addProduct(creds)),
  }
}

const mapStateToProps = ({ addProductState }) => ({
  addProductState,
});

export default @connect(mapStateToProps, mapDispatchToProps)
@withStyles(MainStyle)
class AddProductsBar extends React.Component {
  state = {
    product: '',
    date: new Date(),
    description: '',
    categories: '',
    amount: '',
    error: false,
    open: false,
  };

  handleClose = (event) => {
    this.setState({ error: false });
  };

  handleResetDescription = () =>{
    this.setState({open: false, description: ''})
  }

  toggleModal = () => {
    this.setState({ open: !this.state.open });
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = (event) => {
    this.setState({ date: event });
  }

  resetState() {
    this.setState({
      product: '',
      date: new Date(),
      description: '',
      categories: '',
      amount: ''
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const { product, date, description, categories, amount } = this.state;

    if (categories === '' || date === '') {
      this.setState({ error: true });
      return;
    }

    this.props.addProduct({
      product: Boolean(product) ? product : 'Любой',
      date: date.getTime(),
      description,
      categories,
      amount
    });

    this.resetState();
  };

  render() {
    const { classes, addProductState } = this.props;

    const disable = addProductState === 'request';

    return (
      <Grid item md={12}>
        <form className={classes.container} onSubmit={this.handleSubmit}>
          <Grid container spacing={24} justify="center">
            <Grid item md={12}>
              <Paper className={classes.root} elevation={1}>
                  <SelectField
                    name={'categories'}
                    title={'Категории'}
                    products={categories}
                    onChange={this.handleChange}
                    value={this.state.categories}
                    disabled={disable}
                  />

                  <SelectField
                    name={'amount'}
                    title={'Количество'}
                    products={amount}
                    onChange={this.handleChange}
                    value={this.state.amount}
                    disabled={disable}
                  />

                  <DatePicker
                    date={this.state.date}
                    onChange={this.handleDateChange}
                  />

                  <Button
                    variant="outlined"
                    color="inherit"
                    onClick={this.toggleModal}
                    className={classes.formControl}
                    disabled={disable}
                  >
                    Описание
                  </Button>

                  <Modal
                    onClose={this.toggleModal}
                    onChange={this.handleChange}
                    open={this.state.open}
                    value={this.state.description}
                    onReset={this.handleResetDescription}
                    title={'Добавьте описание вашего продукта'}
                    label={'Описание'}
                  />
                  <Button
                    type='submit'
                    variant="contained"
                    color="inherit"
                    className={classes.button}
                    disabled={disable}
                  >
                    Добавить
                    <Send className={classes.sendIcon} />
                  </Button>

                  <SnackError
                    open={this.state.error}
                    onClose={this.handleClose}
                    title={'Выберите категорию продукта'}
                    />
              </Paper>
            </Grid>
            {this.state.categories !== '' &&
            (<Grid item xs={12} sm={12} md={12} lg={12}>
              <Paper className={classes.root} elevation={1}>
                <SelectField
                  name={'product'}
                  title={'Товары'}
                  products={products[this.state.categories]}
                  onChange={this.handleChange}
                  value={this.state.product}
                  disabled={disable}
                  fullWidth
                  />
              </Paper>
            </Grid>)}
          </Grid>
        </form>
      </Grid>
    );
  }
};
