import React from "react";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, DatePicker } from "material-ui-pickers";
import { InlineDatePicker } from "material-ui-pickers";
// material core
import withStyles from "@material-ui/core/styles/withStyles";
import FormControl from '@material-ui/core/FormControl';
//styles
import SelectFieldStyles from './style';

export default @withStyles(SelectFieldStyles)
class SelectField extends React.Component {
  render() {
    const {
      date,
      onChange,
      classes,
    } = this.props;

    return (
      <FormControl
        variant="outlined"
        className={classes.formControl}
        >
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <div className="pickers">
            <InlineDatePicker
              onlyCalendar
              variant="outlined"
              value={date}
              onChange={onChange}
            />
          </div>
        </MuiPickersUtilsProvider>
      </FormControl>
    );
  }
};
