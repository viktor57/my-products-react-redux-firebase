import { blackColor, hexToRgb } from "../../assets/styles/material-dashboard-react.jsx";

const headerStyle = theme => ({
  searchWrapper: {
    width: '100%',
    textAlign: 'auto'
  },
  search: {
    flex: '0 400px',
    position: 'relative',
  },
  cssLabel: {
   '&$cssFocused': {
     color: "rgba(" + hexToRgb(blackColor) + ", 0.7)",
   },
  },
  notchedOutline: {},
  cssFocused: {},
  cssOutlinedInput: {
   '&$cssFocused $notchedOutline': {
     borderColor: "rgba(" + hexToRgb(blackColor) + ", 0.7)",
   },
  },
});

export default headerStyle;
