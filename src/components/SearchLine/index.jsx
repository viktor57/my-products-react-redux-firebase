import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Tooltip from '@material-ui/core/Tooltip';
// @material-ui/icons
import TextField from '@material-ui/core/TextField';

import styles from "./styles";

export default @withStyles(styles)
class SearchLine extends React.Component {
  render() {
    const {
      classes,
      onChange,
      label,
      error,
      tooltip,
      placementTooltip
    } = this.props;

    return (
      <div className={classes.searchWrapper} id="searchWrapper">
        <div className={classes.search} id="search">
          <Tooltip
            title={tooltip}
            enterDelay={500}
            interactive
            placement={placementTooltip}
          >
            <TextField
              InputLabelProps={{
                classes: {
                  root: classes.cssLabel,
                  focused: classes.cssFocused,
                },
              }}
              InputProps={{
                classes: {
                  root: classes.cssOutlinedInput,
                  focused: classes.cssFocused,
                  notchedOutline: classes.notchedOutline,
                },
              }}
              label={label}
              variant="outlined"
              id="custom-css-outlined-input"
              fullWidth
              onChange={onChange}
              name={'search'}
              error={error}
            />
          </Tooltip>
        </div>
      </div>
    );
  }
};
