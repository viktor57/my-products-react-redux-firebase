import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const titles = {
  fridge: 'В вашем холодильнике ничего нет',
  product: 'В вашем списке покупок пока ничего нет',
  notifications: 'Можно спать спокойно. Уведомлений нет.'
}

export default function WithoutValues({ type, fullWidth }) {
  return (
    <Grid item md={fullWidth ? 12 : 6}>
      <Grid container style={{height: '400px'}} alignItems='center' justify='center'>
        <Typography variant="h5" align="center">
          {titles[type]}
        </Typography>
      </Grid>
    </Grid>
  );
}
