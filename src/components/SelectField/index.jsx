import ReactDOM from 'react-dom';
import React from "react";
import classNames from 'classnames';
// material core
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import OutlinedInput from '@material-ui/core/OutlinedInput';
//styles
import SelectFieldStyles from './style';

export default @withStyles(SelectFieldStyles)
class SelectField extends React.Component {
  state = {
    labelWidth: 0,
  };

  componentDidMount() {
    this.setState({
      labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
    });
  }

  render() {
    const {
      products,
      name,
      title,
      fullWidth,
      onChange,
      value,
      classes,
      disabled
    } = this.props;

    return (
      <FormControl
        variant="outlined"
        className={classNames({[classes.formControl]: !fullWidth})}
        disabled={disabled}
        fullWidth={fullWidth}
        >
        <InputLabel
          ref={ref => {
            this.InputLabelRef = ref;
          }}
          htmlFor={name}
        >
          {title}
        </InputLabel>
        <Select
          value={value}
          onChange={onChange}
          input={
            <OutlinedInput
              labelWidth={this.state.labelWidth}
              name={name}
              id={name}
            />
          }
        >
          {products.map((value, key) => {
            return <MenuItem value={value} key={key}>{value}</MenuItem>;
          })}
        </Select>
      </FormControl>
    );
  }
};
