import React from "react";
import classnames from 'classnames';
import { connect } from 'react-redux';
import withStyles from "@material-ui/core/styles/withStyles";
import { removeProduct } from '../../store/actions/productActions';
import { addProductToFridge } from '../../store/actions/fridgeActions';
import getStrDate from '../../utils';
// @material-ui/core components
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Grid from '@material-ui/core/Grid';
import { Card as CardUI } from '@material-ui/core';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import Switch from '@material-ui/core/Switch';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
// @material-ui/icons icons
import Description from '@material-ui/icons/Description';
import DeleteIcon from '@material-ui/icons/Delete';
import Image from '@material-ui/icons/Image';
// data
import expirations from '../../dataStorage/dateExpiration';
//styles
import cardStyle from './style';
// Images
import milkImg from '../../assets/images/milk.jpg'

const mapDispatchToProps = (dispatch) => {
  return {
    removeProduct: (creds) => dispatch(removeProduct(creds)),
    addProductToFridge: (creds) => dispatch(addProductToFridge(creds)),
  }
}

export default @connect(null, mapDispatchToProps)
@withStyles(cardStyle)
class Card extends React.Component {
  state = {
    value: 0,
    checkedA: false
  };

  handleChangeCheck = name => event => {
    this.setState({ [name]: event.target.checked });
  };

  handleChange = (event, value) => {
    if (value === 2) return;
    this.setState({ value });
  };

  handleRemoveItem = () => {
    this.props.removeProduct([this.props.id]);
  };

  handeAddToFridge = () => {
    const {
      categories,
      date,
      description,
      product,
      amount,
      id
    } = this.props;

    const objDate = new Date(date);

    const dateExpiration = objDate.setDate(objDate.getDate() + expirations[categories][product]);

    const newProductForFridge = {
      categories,
      date,
      description,
      product,
      amount,
      id,
      dateExpiration,
    };

    this.props.addProductToFridge(newProductForFridge);
    this.props.removeProduct([this.props.id]);
  }

  render() {
    const {
      categories,
      date,
      description,
      product,
      amount,
      classes
    } = this.props;

    const { value } = this.state;

    const title = (
      <CardContent>
        {product && (
          <Typography component="p" align="center" variant="overline">
            <span style={{fontWeight: 600}}>Марка: </span>{product}
          </Typography>
        )}
        {description && (
          <Typography component="p" align="center" variant="overline">
            <span style={{fontWeight: 600}}>Описание: </span>{description}
          </Typography>
        )}
        {amount && (
          <Typography component="p" align="center" variant="overline">
            <span style={{fontWeight: 600}}>Количество: </span>{amount}
          </Typography>
        )}
        {(!product && !description && !amount) && (
          <Typography align="center" variant="overline">
            Для этого товара нет описаний
          </Typography>
        )}
      </CardContent>
    );

    const img = (
      <CardMedia
        component="img"
        alt="Contemplative Reptile"
        height="140"
        image={milkImg}
        title="Contemplative Reptile"
        className={classnames({[classes.img]: this.state.checkedA})}
      />
    );

    const action = (
      <Switch
        checked={this.state.checkedA}
        onChange={this.handleChangeCheck('checkedA')}
        value="checkedA"
      />
    );

    return (
      <Grid item md={4} lg={3} xl={2} style={{maxWidth: '300px'}}>
        <CardUI className={classnames({[classes.background]: this.state.checkedA})}>
          <CardContent className={classes.rootHeader}>

            <Tooltip
              title="Добавить в холодильник"
              enterDelay={500}
              interactive
            >
              <Button style={{padding: 0}} onClick={this.handeAddToFridge}>
                <Avatar aria-label="Recipe" className={classes.avatar}>
                  {categories[0]}
                </Avatar>
              </Button>
            </Tooltip>

            <div>
              <Typography
                component="p"
                align="center"
                variant="overline"
                className={classes.headerText}
                style={{fontWeight: 'bold'}}
              >
                {categories}
              </Typography>
              <Typography
                component="p"
                align="center"
                variant="overline"
                className={classes.headerText}
              >
                {getStrDate(new Date(date))}
              </Typography>
            </div>

            <Tooltip
              title="Удалить товар из списка"
              enterDelay={500}
              interactive
            >
              <IconButton
                onClick={this.handleRemoveItem}
                className={classes.removeBtn}
              >
                <DeleteIcon />
              </IconButton>
            </Tooltip>

          </CardContent>

          <Divider />

          <div className={classes.body}>
            {Boolean(this.state.value) ? title : img}
          </div>

          <Divider />

          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            variant="fullWidth"
            indicatorColor="secondary"
            textColor="secondary"
          >
          <Tooltip
            title="Фото"
            placement="bottom"
            enterDelay={500}
            interactive
          >
            <Tab icon={<Image />} className={classes.tabs} />
          </Tooltip>
          <Tooltip
            title="Описание"
            placement="bottom"
            enterDelay={500}
            interactive>
            <Tab icon={<Description />} className={classes.tabs} />
          </Tooltip>
          </Tabs>
        </CardUI>
      </Grid>
    );
  }
};
