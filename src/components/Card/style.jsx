import { blackColor, hexToRgb } from "../../assets/styles/material-dashboard-react.jsx";

const CardStyle = theme => ({
  background: {
    backgroundColor: "rgba(" + hexToRgb(blackColor) + ", 0.8)",
  },
  img: {
    opacity: 0.1
  },
  tabs: {
    minWidth: 'auto',
  },
  avatar: {
    backgroundColor: "rgba(" + hexToRgb(blackColor) + ", 0.7)",
    margin: '12px'
  },
  rootHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '5px'
  },
  headerText: {
    lineHeight: '1.5'
  },
  body: {
    minHeight: '140px',
    display: 'flex',
    justifyContent: 'center'
  }
});

export default CardStyle;
