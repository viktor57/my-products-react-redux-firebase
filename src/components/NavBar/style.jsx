import {
  whiteColor,
  blackColor,
  hexToRgb
} from "../../assets/styles/material-dashboard-react.jsx";

const headerStyle = theme => ({
  buttonsTitle: {
    paddingLeft: '10px'
  },
  appBar: {
    width: '100%',
    padding: '0 20px',
    backgroundColor: whiteColor,
    color: "rgba(" + hexToRgb(blackColor) + ", 0.7)",
    boxShadow: 'none',
    "&:after": {
      content: '""',
      position: "relative",
      bottom: "0",
      height: "2px",
      width: "100%",
      backgroundColor: "rgba(" + hexToRgb(blackColor) + ", 1)"
    }
  },
  root: {
    width: '100%',
    position: 'absolute'
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
     display: 'block',
     color: "rgba(" + hexToRgb(blackColor) + ", 1)"
    },
  },
  searchWrapper: {
    flex: '1',
    display: 'flex',
    justifyContent: 'flex-end',
    [theme.breakpoints.down('sm')]: {
     display: 'block',
    },
  },
  search: {
    flex: '0 400px',
    position: 'relative',
  },
  cssLabel: {
   '&$cssFocused': {
     color: "rgba(" + hexToRgb(blackColor) + ", 0.7)",
   },
  },
  notchedOutline: {},
  cssFocused: {},
  cssOutlinedInput: {
   '&$cssFocused $notchedOutline': {
     borderColor: "rgba(" + hexToRgb(blackColor) + ", 0.7)",
   },
  },
  sectionDesktop: {
    paddingLeft: '10px',
    display: 'none',
    [theme.breakpoints.up('md')]: {
     display: 'flex',
    },
  },
  sectionMobile: {
   display: 'flex',
   [theme.breakpoints.up('md')]: {
     display: 'none',
   },
  },
  link: {
    textDecoration: 'none',
    color: 'black',
    outline: 'none'
  }
});

export default headerStyle;
