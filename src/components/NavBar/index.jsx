import React from "react";
import { connect } from 'react-redux';
import { NavLink } from "react-router-dom";
import { Redirect } from 'react-router-dom';
import { withRouter } from "react-router";
import { compose } from 'redux';
import { signOut } from '../../store/actions/authActions';
import path from 'path';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Button from '@material-ui/core/Button';
// @material-ui/icons
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import TextField from '@material-ui/core/TextField';
import countNotifications from '../../selectors';

import NavBarStyle from "./style";

const mapDispatchToProps = (dispatch) => {
  return {
    signOut: (creds) => dispatch(signOut(creds)),
  }
}

const mapStateToProps = state => ({
  counter: countNotifications(state).length,
  auth: state.firebase.auth
})

const titles = {
  dashboard: 'Главная',
  list: 'Список покупок',
  products: 'Мои продукты',
  notifications: 'События',
  user: 'Профиль',
};

export default @compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)
@withStyles(NavBarStyle)
class NavBar extends React.Component {
  state = {
    anchorEl: null,
    redirect: false,
    count: 0,
    redirect: false,
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
  };

  handleLogOut = () => {
    this.props.signOut();

    this.handleMenuClose();

    this.props.history.push('/signin');

    // this.setState({ redirect: '/signin' });
  }

  // componentDidUpdate(prevProps, prevState) {
  //   if (Boolean(prevState.redirect)) {
  //     this.setState({ redirect: false });
  //   }
  // }

  handleRedirect = () => {
    this.props.history.push('/notifications');
    // this.setState({ redirect: '/notifications' });
  }

  render() {
    const { anchorEl } = this.state;
    const { classes, counter, auth, location } = this.props;

    const isMenuOpen = Boolean(anchorEl);

    const page = location.pathname.split('/')[1];

    return (
      <div className={classes.root}>
        <AppBar position="relative" className={classes.appBar}>
          <Toolbar style={{ height: '90px', padding: '0' }}>
            {/*Page title*/}
            {auth.uid && (
              <Typography className={classes.title} variant="h5" color="inherit" noWrap>
                {titles[page]}
              </Typography>
            )}

            {/* Заглушка для корректного позиционирования элементов */}
            <div style={{flex: 1}} />

            {/* notifications */}
            {(page !== 'signup' && page !== 'signin') && <div className={classes.sectionDesktop}>
              <Button onClick={this.handleRedirect}>
                <Badge badgeContent={counter > 0 && counter} color="secondary">
                  <NotificationsIcon />
                </Badge>
                <Typography className={classes.buttonsTitle} variant="button" color="inherit" noWrap>
                  УВЕДОМЛЕНИЯ
                </Typography>
              </Button>
              <Button
                onClick={this.handleProfileMenuOpen}
                color="inherit"
                >
                <AccountCircle />
              </Button>
            </div>}

            {/* dropdawnMenu */}
            <div className={classes.sectionMobile}>
              <IconButton
                aria-haspopup="true"
                onClick={this.handleMobileMenuOpen}
                color="inherit">
                <MoreIcon />
              </IconButton>
              <Menu
                anchorEl={anchorEl}
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={isMenuOpen}
                onClose={this.handleMenuClose}
              >
              <NavLink to='/user' className={classes.link}>
                <MenuItem onClick={this.handleMenuClose}>Избранное</MenuItem>
              </NavLink>
              <MenuItem onClick={this.handleLogOut}>Выйти</MenuItem>
              </Menu>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
};
