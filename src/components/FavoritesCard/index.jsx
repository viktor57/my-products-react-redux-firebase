import React from "react";
import { connect } from 'react-redux';
import { removeProductFromObserves } from '../../store/actions/observesActions';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from '@material-ui/core/Grid';
import { Card as CardUI } from '@material-ui/core';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Tooltip from '@material-ui/core/Tooltip';
// @material-ui/icons icons
import Description from '@material-ui/icons/Description';
import Image from '@material-ui/icons/Image';
//styles
import cardStyle from './style';

const mapDispatchToProps = (dispatch) => {
  return {
    removeProductFromObserves: (creds) => dispatch(removeProductFromObserves(creds)),
  }
}

export default @connect(null, mapDispatchToProps)
@withStyles(cardStyle)
class Card extends React.Component {
  handleRemoveItem = () => {
    const { categories, product } = this.props;

    this.props.removeProductFromObserves({
      categories,
      product,
    });
  };

  render() {
    const {
      product,
      categories,
      classes
    } = this.props;

    return (
      <Grid item xs={10} sm={6} md={4} lg={3}>
        <CardUI>
          <CardContent className={classes.rootHeader} style={{padding: '10px'}} >
            <Avatar aria-label="Recipe" className={classes.avatar}>
              {categories[0]}
            </Avatar>
            <div >
              <Typography
                component="p"
                align="center"
                variant="overline"
                className={classes.headerText}
                style={{fontWeight: 'bold'}}
                >
                {categories}
              </Typography>

              <Typography
                component="p"
                align="center"
                variant="overline"
                className={classes.headerText}
                style={{fontSize: '10px'}}
                >
                {product}
              </Typography>
            </div>

            <Tooltip
              title="Удалить товар из списка отслеживаемых"
              enterDelay={500}
              interactive
              >
              <IconButton onClick={this.handleRemoveItem} className={classes.removeBtn}>
                <DeleteIcon />
              </IconButton>
            </Tooltip>

          </CardContent>
        </CardUI>
      </Grid>
    );
  }
};
