import { blackColor, hexToRgb } from "../../assets/styles/material-dashboard-react.jsx";

const CardStyle = theme => ({
  avatar: {
    backgroundColor: "rgba(" + hexToRgb(blackColor) + ", 0.7)",
  },
  rootHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '5px',
    paddingBottom: '5px'
  },
  headerText: {
    lineHeight: '1.5'
  }
});

export default CardStyle;
