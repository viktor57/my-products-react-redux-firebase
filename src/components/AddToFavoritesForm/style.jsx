const CardStyle = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    display: 'flex',
    alignItems: 'center',
    textAlign: 'auto'
  },
  formControl: {
    marginRight: '25px'
  },
  button: {
    margin: theme.spacing.unit,
    width: '200px'
  },
  sendIcon: {
    marginLeft: theme.spacing.unit,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: '100%'
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  modal: {
    width: '80%'
  },
  SnackbarContent: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
});

export default CardStyle;
