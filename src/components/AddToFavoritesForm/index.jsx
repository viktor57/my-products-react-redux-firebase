import React from "react";
import { connect } from 'react-redux';
import { addProductToObserves } from '../../store/actions/observesActions';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Send from '@material-ui/icons/Send';
// Core components
import Modal from '../Modal';
import SelectField from '../SelectField';
import SnackError from '../SnackError';
// data
import categories from '../../dataStorage/categories';
import products from '../../dataStorage/products';
//styles
import MainStyle from './style';

const mapDispatchToProps = (dispatch) => {
  return {
    addProductToObserves: (creds) => dispatch(addProductToObserves(creds)),
  }
}

const mapStateToProps = ({ addProductToObservesState }) => ({
  addProductToObservesState,
});

export default @connect(mapStateToProps, mapDispatchToProps)
@withStyles(MainStyle)
class AddProductsBar extends React.Component {
  state = {
    open: false,
    product: '',
    categories: '',
    error: ''
  };

  handleClose = (event) => {
    this.setState({ error: false });
  };

  handleResetDescription = () =>{
    this.setState({open: false, description: ''})
  }

  toggleModal = () => {
    this.setState({ open: !this.state.open });
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  resetState() {
    this.setState({
      product: '',
      categories: '',
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const { product, categories } = this.state;

    if (categories === '') {
      this.setState({ error: 'Выберите категорию товара' });
      return;
    }

    this.props.addProductToObserves({
      product: product === '' ? 'Любые' : product,
      categories,
    })
    .then(() => this.resetState())
    .catch(e => this.setState({ error: 'Проверьте что товара нет среди выбранных' }));
  };

  render() {
    const { classes, addProductToObservesState } = this.props;

    const disable = addProductToObservesState === 'request';

    return (
      <Grid item xs={12} sm={12} md={12} lg={12}>
        <form className={classes.container} onSubmit={this.handleSubmit}>
          <Grid container spacing={24} justify="center">
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <Paper className={classes.root} elevation={1}>
                  <SelectField
                    name={'categories'}
                    title={'Категории'}
                    products={categories}
                    onChange={this.handleChange}
                    value={this.state.categories}
                    disabled={disable}
                  />
                  <SelectField
                    name={'product'}
                    title={'Товары'}
                    products={products[this.state.categories] || ['Выберите категорию']}
                    onChange={this.handleChange}
                    value={this.state.product}
                    disabled={disable}
                  />
                  {/* Заглушка для корректного позиционирования элементов */}
                  <div style={{flex: 1}} />
                  <Button
                    type='submit'
                    variant="contained"
                    color="inherit"
                    className={classes.button}
                    disabled={disable}
                  >
                    Добавить
                    <Send className={classes.sendIcon} />
                  </Button>
                  <SnackError
                    open={Boolean(this.state.error)}
                    onClose={this.handleClose}
                    title={this.state.error}
                    />
              </Paper>
            </Grid>
          </Grid>
        </form>
      </Grid>
    );
  }
};
