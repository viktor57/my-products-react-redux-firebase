import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
// utils
import getStrDate from '../../utils';
// material core
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import AssessmentIcon from '@material-ui/icons/Assessment';
// core
import EnhancedTableHead from './EnhancedTableHead';
import EnhancedTableToolbar from './EnhancedTableToolbar';
import Modal from '../Modal';
// actions
import { removeProduct } from '../../store/actions/productActions';
import { removeProductFromFridge } from '../../store/actions/fridgeActions';

import styles from './styles';

// Функция сортировки для .sort() по возрастанию
function desc(a, b, orderBy) {
  return b[orderBy] - a[orderBy];
}

// функция сортировки массива
function stableSort(array, cmp) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = cmp(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

// декоратор для функции .sort()
function getSorting(order, orderBy) {
  return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const mapDispatchToProps = (dispatch) => {
  return {
    removeProduct: (creds) => dispatch(removeProduct(creds)),
    removeProductFromFridge: (creds) => dispatch(removeProductFromFridge(creds)),
  }
}

export default @connect(null, mapDispatchToProps)
@withStyles(styles)
class EnhancedTable extends React.Component {
  state = {
    order: 'asc',
    orderBy: 'categories',
    selected: [],
    page: 0,
    rowsPerPage: 5,
    modal: {},
    placeholders: {
      product: {
        amount: 'Любое',
        product: 'Любой'
      },
      fridge: {
        amount: 'Неизвестно',
        product: 'Нет'
      }
    }
  };

  toggleModal = (id) => () => {
    this.setState({ modal: { ...this.state.modal, [id]: !this.state.modal[id] }});
  }

  // запрос на сортировку по возрастанию или убыванию
  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }

    this.setState({ order, orderBy });
  };

  // если отмечен главный checkbox, отмечаем все остальные
  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: this.props.values.map(n => n.id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (id) => (event) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  handleRemoveItems = () => {
    const { removeProduct, removeProductFromFridge, type } = this.props;
    const { selected } = this.state;

    type === 'product' ? this.props.removeProduct(selected) : this.props.removeProductFromFridge(selected);
    this.setState({ selected: [] })
  }

  render() {
    const data = this.props.values || [];
    const { classes, type, fullWidth, cells } = this.props;
    const { order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - page * rowsPerPage);

    return (
      <Grid item md={fullWidth ? 12 : 6}>
        <Paper className={classes.root}>
          <EnhancedTableToolbar
            numSelected={selected.length}
            onRemoveItems={this.handleRemoveItems}
            title={type === 'product' ? 'Cписок покупок' : 'Продукты в холодильнике'}
          />
        <div className={classes.tableWrapper}>
            <Table className={classes.table} aria-labelledby="tableTitle">
              <EnhancedTableHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={data.length}
                type={type}
                cells={cells}
                fullWidth={fullWidth}
              />
              <TableBody>
                {stableSort(data, getSorting(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map(n => {
                    const isSelected = this.isSelected(n.id);
                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        aria-checked={fullWidth && isSelected}
                        tabIndex={-1}
                        key={n.id}
                        selected={fullWidth && isSelected}
                      >
                        {fullWidth && (
                          <TableCell padding="checkbox" onClick={this.handleClick(n.id)}>
                            <Checkbox checked={isSelected} />
                          </TableCell>
                        )}
                        {cells.map(cell => {
                          if (cell === 'description') return;
                          if (cell === 'date' || cell === 'dateExpiration') {
                            return (
                              <TableCell key={cell} component="th" scope="row" style={{paddingLeft: '20px'}}>
                                {Boolean(n[cell]) ? getStrDate(new Date(n[cell])) : this.state.placeholders[type][cell]}
                              </TableCell>
                            );
                          }
                          return (
                            <TableCell key={cell} component="th" scope="row" style={{paddingLeft: '20px'}}>
                              {Boolean(n[cell]) ? n[cell] : this.state.placeholders[type][cell]}
                            </TableCell>
                          );
                        })}
                        <TableCell key={'description'} component="th" scope="row" style={{paddingLeft: '20px'}}>
                          <Tooltip
                            title="Просмотр описания"
                            enterDelay={500}
                            interactive
                            >
                            <IconButton onClick={this.toggleModal(n.id)}>
                              <AssessmentIcon />
                              <Modal
                                onClose={this.toggleModal(n.id)}
                                onChange={() => {}}
                                open={Boolean(this.state.modal[n.id])}
                                onReset={() => {}}
                                title={'Описание вашего продукта'}
                                label={'Описание'}
                                content={n}
                                />
                            </IconButton>
                          </Tooltip>
                        </TableCell>
                      </TableRow>
                    );
                  })}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </div>

          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={data.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </Paper>
      </Grid>
    );
  }
};
