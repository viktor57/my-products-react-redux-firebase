import React from 'react';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Tooltip from '@material-ui/core/Tooltip';

const rows = {
  fridge: [
    { id: 'categories', numeric: false, disablePadding: true, label: 'Категория' },
    { id: 'product', numeric: false, disablePadding: true, label: 'Марка' },
    { id: 'dateExpiration', numeric: false, disablePadding: true, label: 'Срок годности истекает' },
    { id: 'amount', numeric: true, disablePadding: false, label: 'Количество' },
    { id: 'description', numeric: true, disablePadding: false, label: 'Описание' },
    { id: 'shelfLife', numeric: true, disablePadding: false, label: 'Срок годности' },
  ],
  product: [
    { id: 'categories', numeric: false, disablePadding: true, label: 'Категория' },
    { id: 'product', numeric: false, disablePadding: true, label: 'Название продукта' },
    { id: 'date', numeric: true, disablePadding: false, label: 'Дата покупки' },
    { id: 'amount', numeric: true, disablePadding: false, label: 'Количество' },
    { id: 'description', numeric: true, disablePadding: false, label: 'Описание' },
  ]
}

export default class EnhancedTableHead extends React.Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount,
      type,
      cells,
      fullWidth
    } = this.props;

    return (
      <TableHead>
        <TableRow>
          {fullWidth && (
            <Tooltip
              title={'Выбрать все продукты'}
              enterDelay={500}
              interactive
            >
              <TableCell padding="checkbox">
                <Checkbox
                  indeterminate={numSelected > 0 && numSelected < rowCount}
                  checked={numSelected === rowCount && numSelected !== 0}
                  onChange={onSelectAllClick}
                />
              </TableCell>
            </Tooltip>
            )}
          {rows[type].map(row => {
            if (cells.includes(row.id)) {
              return (
                <TableCell
                  key={row.id}
                  component="th" padding="none"
                  style={{paddingLeft: '20px'}}
                  sortDirection={orderBy === row.id ? order : false}
                >
                  <Tooltip
                    title="Sort"
                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                    enterDelay={300}
                  >
                    <TableSortLabel
                      active={orderBy === row.id}
                      direction={order}
                      onClick={this.createSortHandler(row.id)}
                    >
                      {row.label}
                    </TableSortLabel>
                  </Tooltip>
                </TableCell>
              )
            }}
          )}
        </TableRow>
      </TableHead>
    );
  }
}
