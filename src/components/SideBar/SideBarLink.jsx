import React from "react";
import { NavLink } from "react-router-dom";
// @material-ui/core components
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Icon from "@material-ui/core/Icon";
import withStyles from "@material-ui/core/styles/withStyles";
// Styles
import SideBarLinkStyle from "./style";

export default @withStyles(SideBarLinkStyle)
class SideBarLink extends React.Component {
  handleClick = (event) => {
    if (!Boolean(this.props.auth)) {
      event.preventDefault();
    }
  }

  render() {
    const { prop, classes } = this.props;

    return (
      <NavLink to={prop.path} activeClassName="active" className={classes.item} onClick={this.handleClick}>
        <ListItem button className={classes.itemLink}>
          {typeof prop.icon === "string" ? (
            <Icon className={classes.itemIcon}>
              {prop.icon}
            </Icon>
          ) : (
            <prop.icon className={classes.itemIcon}/>
          )}
          <ListItemText
            primary={prop.name}
            disableTypography={true}
            className={classes.itemText}
          />
        </ListItem>
      </NavLink>
    );
  }
};
