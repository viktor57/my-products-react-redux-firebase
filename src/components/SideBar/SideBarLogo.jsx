import React from "react";
import { NavLink } from "react-router-dom";
import withStyles from "@material-ui/core/styles/withStyles";
import SideBarLogoStyle from "./style";
import logo from '../../assets/images/logo.png';

const appName = 'my products';

export default @withStyles(SideBarLogoStyle)
class SideBarLogo extends React.Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.logo}>
        <NavLink to={'/dashboard'} className={classes.logoLink}>
          <div className={classes.logoImage}>
            <img src={logo} alt="logo" className={classes.img}/>
          </div>
          {appName.toUpperCase()}
        </NavLink>
      </div>
    );
  }
};
