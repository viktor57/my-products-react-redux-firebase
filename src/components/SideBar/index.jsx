import React from "react";
import { connect } from 'react-redux';
// components
import SideBarLink from './SideBarLink';
import SideBarLogo from './SideBarLogo';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
// Scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// styles
import sidebarStyle from "./style";
// background for sideBar
import background from '../../assets/images/pensils_1.jpg';

const mapStateToProps = state => ({
  auth: state.firebase.auth.uid
});

export default @connect(mapStateToProps)
@withStyles(sidebarStyle)
class Sidebar extends React.Component {
  componentDidMount() {
    const ps = new PerfectScrollbar('#SideBar');
  }

  render() {
    const { classes, routes, auth } = this.props;
  
    return (
      <Hidden smDown implementation="css">
        <Drawer
          variant="permanent"
          open
          classes={{
            paper: classes.drawerPaper
          }}>

          <SideBarLogo />

          <div className={classes.sidebarWrapper} id="SideBar">
            <List className={classes.list}>
              {routes.map((prop, key) => {
                return (
                  <SideBarLink
                    auth={auth}
                    key={key}
                    prop={prop}
                  />
                )
              })}
            </List>
          </div>

          <div
            style={{ backgroundImage: "url(" + background + ")" }}
            className={classes.background}
          />
        </Drawer>
      </Hidden>
    );
  }
};
