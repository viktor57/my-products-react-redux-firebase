const notificiationStyle = theme => ({
  root: {
    minHeight: '150px',
    display: 'flex',
    flexDirection: 'column',
    marginTop: '20px'
  },
  container: {
    height: '108px',
    display: 'flex',
    position: 'relative',
    flex: '1',
    alignItems: 'center'
  },
  square: {
    width: '50%',
    height: '100%',
    position: 'relative',
  },
  decoration: {
    width: '90px',
    height: '90px',
    position: 'relative',
    top: '-20px',
    left: '20px',
    borderRadius: '3px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  footer: {
    padding: '5px 0',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    boxSizing: 'border-box',
    width: '50%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  header: {
    height: '30px',
  },
  intoPaper: {
    width: '80%',
    height: '80%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default notificiationStyle;
