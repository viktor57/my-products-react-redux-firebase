import React from 'react';
import { Redirect } from 'react-router-dom';
// material
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Announcement from '@material-ui/icons/Announcement';
import Clear from '@material-ui/icons/Clear';
import ReportProblem from '@material-ui/icons/ReportProblem';
import ErrorOutline from '@material-ui/icons/ErrorOutline';
import AccessAlarm from '@material-ui/icons/AccessAlarm';
import Report from '@material-ui/icons/Report';
import Button from '@material-ui/core/Button';
import Tooltip from '@material-ui/core/Tooltip';
// core
import Modal from '../Modal';
// style
import styles from './styles';

const problems = {
  amount: {
    title: 'Продукт заканчивается',
    color: <Report style={{color: 'gray', paddingLeft: '10px', paddingRight: '10px'}} />,
    decoration: 'blue',
    icon: <ErrorOutline style={{color: 'white', fontSize: '40px'}} />
  },
  purchase: {
    title: 'Необходимо купить',
    color: <AccessAlarm style={{color: '#FFCE18', paddingLeft: '10px', paddingRight: '10px'}} />,
    decoration: 'purple',
    icon: <Announcement style={{color: 'white', fontSize: '40px'}} />
  },
  shelfLife: {
    title: 'Истекает срок годности',
    color: <ReportProblem style={{color: 'red', paddingLeft: '10px', paddingRight: '10px'}} />,
    decoration: 'green',
    icon: <Clear style={{color: 'white', fontSize: '40px'}} />
  }
}

export default @withStyles(styles)
class NotificiationCard extends React.Component {
  state = {
    open: false,
    redirect: false,
  }

  toggleModal = () => {
    this.setState({ open: !this.state.open });
  }

  handleClick = () => {
    this.setState({ open: true });
  }

  handleClickTo = () => {
    const { type } = this.props;

    if (type === 'purchase') {
      this.setState({ redirect: '/list' });
    } else {
      this.setState({ redirect: '/products' })
    }
  }

  render() {
    const { classes, item } = this.props;
    const { type, categories, product } = item;

    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }

    return (
      <Grid item md={4} lg={3} xl={2} >
        <Paper elevetaion={1} className={classes.root}>
          <Modal
            onClose={this.toggleModal}
            onChange={() => {}}
            open={Boolean(this.state.open)}
            onReset={() => {}}
            title={'Описание вашего продукта'}
            label={'Описание'}
            content={item}
            />
          <div className={classes.container}>
            <div className={classes.square} >
              <Tooltip
                title="Перейти к списку продуктов"
                enterDelay={500}
                interactive
                >
                <Button
                  onClick={this.handleClickTo}
                  className={classes.decoration}
                  style={{backgroundColor: problems[type].decoration }}>
                  {problems[type].icon}
                </Button>
              </Tooltip>
            </div>
            <div className={classes.title}>
              <Paper elevetaion={1} className={classes.intoPaper}>
                <Tooltip
                  title="Перейти к описанию товара"
                  enterDelay={500}
                  interactive
                  >
                  <Button
                    style={{width: '100%', height: '100%'}}
                    onClick={this.handleClick}>
                    <Typography
                      component="p"
                      align="center"
                      variant="overline"
                      style={{fontSize: '11px', fontWeight: '600'}}>
                      {`${categories}`}
                    </Typography>
                  </Button>
                </Tooltip>
              </Paper>
            </div>
          </div>
          <Divider />
          <div className={classes.footer}>
            {problems[type].color}
            <Typography component="p" align="center" variant="overline">
              {problems[type].title}
            </Typography>
          </div>
        </Paper>
      </Grid>
    );
  }
}
