import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import ErrorIcon from '@material-ui/icons/Error';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';

const SnackbarContentStyle = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center'
};

export default function SnackError(props) {
  const { open, onClose, onClick, title } = props;

  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      autoHideDuration={5000}
      onClose={onClose}
      open={open}
    >
      <SnackbarContent
        aria-describedby="message-id"
        message={(
          <div style={SnackbarContentStyle} id="message-id">
            <ErrorIcon style={{paddingRight: '20px'}}/>
            <span>{title}</span>
          </div>
        )}
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            onClick={onClose}
          >
            <CloseIcon />
          </IconButton>,
        ]}
        />
    </Snackbar>
  );
}
