import React from "react";
// material core
import withStyles from "@material-ui/core/styles/withStyles";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Slide from '@material-ui/core/Slide';
import DialogContentText from '@material-ui/core/DialogContentText';
import Typography from '@material-ui/core/Typography';
// utils
import getStrDate from '../../utils';
//styles
import ModalStyles from './style';

function Transition(props) {
  return <Slide direction="down" {...props} />;
}

const titles = {
  categories: 'Продукт: ',
  product: 'Марка: ',
  amount: 'Количество: ',
  date: 'Дата покупки: ',
  description: 'Описание: ',
  dateExpiration: 'Окончание срока годности: '
}

export default @withStyles(ModalStyles)
class Modal extends React.Component {
  render() {
    const {
      onClose,
      onChange,
      onReset,
      title,
      open,
      value,
      label,
      classes,
      content
    } = this.props;

    return (
      <Dialog
        id={'modal'}
        open={open}
        onClose={onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        fullWidth
        TransitionComponent={Transition}
      >
        <DialogTitle id="alert-dialog-title" className={classes.dialogTitle}>
          {title}
        </DialogTitle>
        <DialogContent className={classes.dialogContent}>
            {content ? (
              <DialogContentText>
                {Object.keys(titles).map((title, index) => {
                  if (!Boolean(content.dateExpiration) && title === 'dateExpiration') return;

                  const withDateTitle = (Boolean(content.dateExpiration) && title === 'dateExpiration') || (title === 'date');

                  if (withDateTitle) {
                    return (
                      <Typography key={index} align="center" variant="overline">
                        <span style={{fontWeight: 600}}>{titles[title]}</span>{getStrDate(new Date(content[title]))}
                      </Typography>
                    );
                  } else {
                    return content[title] !== '' && (
                      <Typography key={index} align="center" variant="overline">
                        <span style={{fontWeight: 600}}>{titles[title]}</span>{content[title]}
                      </Typography>
                    );
                  }
                })}
              </DialogContentText>
            ) : (
              <TextField
                  value={value}
                  id="standard-name"
                  label={label}
                  name="description"
                  className={classes.textField}
                  onChange={onChange}
                  margin="normal"
                  variant="outlined"
                  multiline
                  rows="4"
                />
            )}
        </DialogContent>
        {!content && (
          <DialogActions>
            <Button onClick={onReset} color="inherit">
              Отменить
            </Button>
            <Button onClick={onClose} variant="outlined" color="inherit" autoFocus>
              Добавить
            </Button>
          </DialogActions>
        )}
      </Dialog>
    );
  }
};
