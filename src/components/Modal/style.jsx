const style = theme => ({
  textField: {
    width: '100%',
  },
  dialogContent: {
    textAlign: 'center',
    overflow: 'hidden',
  },
  dialogTitle: {
    textAlign: 'center',
  }
});

export default style;
