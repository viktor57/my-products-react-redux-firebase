import React from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { withRouter } from "react-router";
import { compose } from 'redux'
// Actions
import { signIn, signUp } from '../../store/actions/authActions';
// core
import SnackError from '../SnackError';
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
// Styles
import signFormStyles from './styles.jsx';

const typeData = {
  signIn: {
    titles: ['Войдите в ваш аккаунт', 'или создайте новый'],
    link: '/signup',
    buttonLabels: ['Войти', 'Зарегистрировать'],
  },
  signUp: {
    titles: ['Зарегистрируйте новый аккаунт', 'и наслаждайтесь покупками'],
    link: '/signin',
    buttonLabels: ['Зарегистрировать', 'Войти'],
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signIn: (creds) => dispatch(signIn(creds)),
    signUp: (creds) => dispatch(signUp(creds)),
  }
}

const mapStateToProps = ({ signState }) => ({
  signState,
});

export default @compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)
@withStyles(signFormStyles)
class SignForm extends React.Component {
  state = {
    email: '',
    password: '',
    firstName: '',
    lastName: '',
    error: false
  };

  isInvalidPassword = () => {
    return (this.state.password.length < 8) && (this.state.password.length !== 0);
  }

  handleSubmit = (event) => {
    event.preventDefault();

    const { type } = this.props;

    if (this.isInvalidPassword() || this.props.signState === 'request') {
      return;
    }

    this.props[type](this.state)
    .then(() => {
      this.props.history.push('/dashboard');
    })
    .catch(e => {
      this.setState({ error: true })
    });
  }

  resetState = () => {
    this.setState({login: '', password: '', name: ''});
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleClose = (event) => {
    this.setState({ error: false });
  };

  render() {
    const { classes, type } = this.props;

    const disabled = this.props.signState === 'request';

    const currentValues = typeData[type];

    const signUpFields = (
      <React.Fragment>
        <TextField
          label="firstName"
          className={classes.textField}
          value={this.state.firstName}
          onChange={this.handleChange}
          margin="normal"
          variant="outlined"
          name="firstName"
          disabled={disabled}
        />
        <TextField
          label="lastName"
          className={classes.textField}
          value={this.state.lastName}
          onChange={this.handleChange}
          margin="normal"
          variant="outlined"
          name="lastName"
          disabled={disabled}
        />
      </React.Fragment>
    );

    return (
      <Grid container spacing={24} justify="center">
        <Grid item className={classes.container}>
          <Grid item className={classes.header}>
            <Typography variant="h4" align="center" className={classes.headerText}>
              {currentValues.titles[0]}
            </Typography>
            <Typography variant="h4" align="center" className={classes.headerText}>
              {currentValues.titles[1]}
            </Typography>
          </Grid>
          <Paper elevation={5} className={classes[`paper${type}`]}>
            <form onSubmit={this.handleSubmit} className={classes.form}>
              <TextField
                label="Email"
                value={this.state.email}
                onChange={this.handleChange}
                margin="normal"
                variant="outlined"
                name="email"
                disabled={disabled}
              />
              <TextField
                label="Password"
                value={this.state.password}
                onChange={this.handleChange}
                margin="normal"
                variant="outlined"
                name="password"
                type="password"
                disabled={disabled}
                error={this.isInvalidPassword()}
              />

              {this.isInvalidPassword() && (
                <Typography
                  variant="subheading"
                  align="center"
                  style={{color: "#333333", fontSize: '13px', position: 'absolute'}}
                  >
                  {'Пароль должен состоять не менее чем из 8 символов'}
                </Typography>
              )}

              {type === 'signUp' && signUpFields}

              <Button
                type={'submit'}
                variant="contained"
                className={classes[`button${type}`]}
                >
                {currentValues.buttonLabels[0]}
              </Button>
              <Typography
                variant="subheading"
                align="center"
                className={classes.headerText}
                style={{color: "#333333"}}
                >
                {'или'}
              </Typography>
              <Link to={currentValues.link} className={classes.link}>
                <Button
                  variant="contained"
                  className={classes.linkButton}
                  >
                {currentValues.buttonLabels[1]}
                </Button>
              </Link>

              <SnackError
                open={this.state.error}
                onClose={this.handleClose}
                title={'Введите корректные данные'}
                />
            </form>
          </Paper>
        </Grid>
      </Grid>
    );
  }
}
