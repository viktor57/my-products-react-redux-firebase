import { blackColor, hexToRgb, grayColor } from '../../assets/mainStyles.jsx';

const backgroundColor = '#4d4d4d';

export default (theme) => ({
  buttonsignIn: {
    margin: theme.spacing.unit,
    backgroundColor,
    color: 'white'
  },
  buttonsignUp: {
    margin: theme.spacing.unit,
    backgroundColor,
    color: 'white',
    marginTop: '20px'
  },
  input: {
    display: 'none',
  },
  form: {
    width: '400px',
    display: 'flex',
    flexDirection: 'column',
    textAlign: 'auto'
  },
  papersignIn: {
    height: '400px',
    width: '600px',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    flex: '1 100%',
    position: 'relative',
    top: '-80px',
    paddingBottom: '50px'
  },
  papersignUp: {
    height: '550px',
    width: '600px',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    flex: '1 100%',
    position: 'relative',
    top: '-80px',
    paddingBottom: '50px'
  },
  header:{
    height: '150px',
    width: '80%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor,
    borderRadius: '5px',
    textAling: 'center',
    flex: '0 80%',
    zIndex: 1,
    boxShadow: '0 0 10px 0 rgba(46, 49, 49, 0.8)'
  },
  headerContainer: {
    width: '80%',
    height: '80%',
    display: 'flex',
  },
  headerText: {
    display: 'block',
    color: 'white',
    fontWeight: 400
  },
  container: {
    display: 'flex',
    position: 'relative',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  link: {
    display: 'flex',
    color: 'white',
    textDecoration: 'none',
    width: '100%',
    marginTop: '10px'
  },
  linkButton: {
    display: 'block',
    color: 'white',
    width: '100%',
    boxSizing: 'border-box',
    backgroundColor,
    margin: '0 8px',
  },
  SnackbarContent: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  }
});
