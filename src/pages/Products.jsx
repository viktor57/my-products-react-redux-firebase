import React from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import AddProductsForm from '../components/AddProductsForm';
import Card from '../components/Card';
import Table from '../components/Table';
import Typography from '@material-ui/core/Typography';
import SearchLine from '../components/SearchLine';
import WithoutValues from '../components/WithoutValues';

const mapStateToProps = (state) => ({
  fridge: state.firebase.profile.fridge,
});


export default @connect(mapStateToProps)
class Products extends React.Component {
  state = {
    search: ''
  };

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value})
  }

  render() {
    const { fridge = [] }  = this.props;

    const actualValues = fridge.filter(item => item.categories.toLowerCase() === this.state.search.toLowerCase());

    return (
      <Grid container spacing={24}>
        <Grid item md={12} style={{paddingBottom: 0, paddingTop: 0}}>
          <SearchLine
            style={{textAlign: 'auto'}}
            onChange={this.handleChange}
            label='Категории товаров'
            tooltip='Фильтр товаров по выбранной категории'
            placementTooltip='bottom-start'
            name='search'
            error={actualValues.length === 0 && this.state.search !== ''}
          />
        </Grid>
        <Grid item md={12} style={{paddingTop: 0, paddingBottom: 0}}>
          {fridge.length === 0 ? (
            (
              <WithoutValues type='fridge' fullWidth />
            )
          ) : (
            <Table
              values={actualValues.length === 0 ? fridge : actualValues}
              type='fridge'
              cells={['categories', 'product', 'dateExpiration', 'amount', 'description']}
              fullWidth
            />
          )}
        </Grid>
      </Grid>
    );
  }
};
