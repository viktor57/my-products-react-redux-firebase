import React from 'react';
import FavoritesCard from '../components/FavoritesCard';
import AddToFavoritesForm from '../components/AddToFavoritesForm';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import { uniqueId } from 'lodash';

const mapStateToProps = (state) => ({
  observes: state.firebase.profile.observes,
});


export default @connect(mapStateToProps)
class User extends React.Component {
  render() {
    const { observes = [] } = this.props;

    return (
      <Grid container spacing={24}>
        <Grid container justify='center'>
          <Paper elevation={1} style={{display: 'flex', alignItems: 'center', justifyContent: 'center', width: '70%', height: '50px'}}>
            <Typography component="p" align="center" variant="overline">
              <span style={{fontWeight: 600}}>Выберите категории продуктов для отслеживания</span>
            </Typography>
          </Paper>
        </Grid>
        <AddToFavoritesForm />
        {observes.map(item => <FavoritesCard key={uniqueId()} { ...item} />)}
      </Grid>
    );
  }
}
