import React from 'react';
import Table from '../components/Table';
import WithoutValues from '../components/WithoutValues';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import AddProductsForm from '../components/AddProductsForm';
import Card from '../components/Card';
import Typography from '@material-ui/core/Typography';

const mapStateToProps = (state) => ({
  products: state.firebase.profile.products,
  fridge: state.firebase.profile.fridge,
});

export default @connect(mapStateToProps)
class Dashboard extends React.Component {
  render() {
    const { products = [], fridge = [] } = this.props;

    return (
      <Grid container spacing={24}>
        {products.length === 0 ? (
          (
            <WithoutValues type='product' />
          )
        ) : (
          <Table values={products} type='product' cells={['categories', 'product', 'date', 'description']} />
        )}
        {fridge.length === 0 ? (
          (
            <WithoutValues type='fridge' />
          )
        ) : (
          <Table values={fridge} type='fridge' cells={['categories', 'dateExpiration', 'amount', 'description']} />
        )}
      </Grid>
    );
  }
};
