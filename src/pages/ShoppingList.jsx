import React from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import AddProductsForm from '../components/AddProductsForm';
import Card from '../components/Card';
import Typography from '@material-ui/core/Typography';
import WithoutValues from '../components/WithoutValues';

const mapStateToProps = (state) => ({
  products: state.firebase.profile.products,
});

const sortByDate = (a, b) => {
  return a.date - b.date;
};

export default @connect(mapStateToProps)
class ShoppingList extends React.Component {
  render() {
    const { products = [] } = this.props;

    return (
      <Grid container spacing={24}>
        <Grid item md={12}>
          <AddProductsForm />
        </Grid>
        {products.length > 0 ?
          products.sort(sortByDate).map((product, key) => {
            return (
              <Card {...product} key={key} />
            );
          })
          :
          (
            <WithoutValues type='product' fullWidth />
          )}
      </Grid>
    );
  }
};
