import React from 'react';
import NotificationsCard from '../components/NotificiationCard';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { uniqueId } from 'lodash';
import WithoutValues from '../components/WithoutValues';
import countNotifications from '../selectors';


const mapStateToProps = state => ({
  notifications: countNotifications(state),
})

export default @connect(mapStateToProps)
class Notifications extends React.Component {
  render() {
    const { notifications } = this.props;

    return (
      <Grid container spacing={24}>
        { notifications.length > 0 ? (
          notifications.map(item => <NotificationsCard item={item} key={uniqueId()} />)
        ): (
          <WithoutValues type='notifications' fullWidth />
        )
        }
      </Grid>
    );
  }
}
