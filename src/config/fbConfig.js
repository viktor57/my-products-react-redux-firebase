import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyD8jzS5wLztHJnzVAdYtsQ52cWx0NkYzyM',
  authDomain: 'my-products-4c808.firebaseapp.com',
  databaseURL: 'https://my-products-4c808.firebaseio.com',
  projectId: 'my-products-4c808',
  storageBucket: 'my-products-4c808.appspot.com',
  messagingSenderId: '833875719651',
};

firebase.initializeApp(config);
firebase.firestore();

export default firebase;
