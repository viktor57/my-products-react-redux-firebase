import { combineReducers } from 'redux';
import { firestoreReducer } from 'redux-firestore';
import { firebaseReducer } from 'react-redux-firebase';
import sign from './authReducer';
import { addProductToFridge } from './fridgeReducer';
import { addProductToObserves } from './observesReducer';
import { addProduct } from './productReducer';

const rootReducer = combineReducers({
  firestore: firestoreReducer,
  firebase: firebaseReducer,
  signState: sign,
  addProductToFridgeState: addProductToFridge,
  addProductToObservesState: addProductToObserves,
  addProductState: addProduct,
});

export default rootReducer;
