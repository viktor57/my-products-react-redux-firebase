import { handleActions } from 'redux-actions';
import * as actions from '../actions';

export const removeProductFromFridge = handleActions({
  [actions.removeProductFromFridgeSuccess]() {
    return 'success';
  },
  [actions.removeProductFromFridgeFailure]() {
    return 'failure';
  },
  [actions.removeProductFromFridgeRequest]() {
    return 'request';
  },
}, 'none');

export const addProductToFridge = handleActions({
  [actions.addProductToFridgeRequest]() {
    return 'request';
  },
  [actions.addProductToFridgeSuccess]() {
    return 'success';
  },
  [actions.addProductToFridgeFailure]() {
    return 'failure';
  },
}, 'none');
