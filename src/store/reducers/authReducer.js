import { handleActions } from 'redux-actions';
import * as actions from '../actions';

export default handleActions({
  [actions.signInSuccess]() {
    return 'success';
  },
  [actions.signInFailure]() {
    return 'failure';
  },
  [actions.signInRequest]() {
    return 'request';
  },
  [actions.signUpRequest]() {
    return 'request';
  },
  [actions.signUpSuccess]() {
    return 'success';
  },
  [actions.signUpFailure]() {
    return 'failure';
  },
}, 'none');
