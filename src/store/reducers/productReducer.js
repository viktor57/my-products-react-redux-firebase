import { handleActions } from 'redux-actions';
import * as actions from '../actions';

export const addProduct = handleActions({
  [actions.addProductSuccess]() {
    return 'success';
  },
  [actions.addProductFailure]() {
    return 'failure';
  },
  [actions.addProductRequest]() {
    return 'request';
  },
}, 'none');

export const removeProduct = handleActions({
  [actions.removeProductRequest]() {
    return 'request';
  },
  [actions.removeProductSuccess]() {
    return 'success';
  },
  [actions.removeProductFailure]() {
    return 'failure';
  },
}, 'none');
