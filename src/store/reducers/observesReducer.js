import { handleActions } from 'redux-actions';
import * as actions from '../actions';

export const addProductToObserves = handleActions({
  [actions.addProductToObservesSuccess]() {
    return 'success';
  },
  [actions.addProductToObservesFailure]() {
    return 'failure';
  },
  [actions.addProductToObservesRequest]() {
    return 'request';
  },
}, 'none');

export const removeProducFromObserves = handleActions({
  [actions.removeProductFromObservesRequest]() {
    return 'request';
  },
  [actions.removeProductFromObservesSuccess]() {
    return 'success';
  },
  [actions.removeProductFromObservesFailure]() {
    return 'failure';
  },
}, 'none');
