import {
  signInSuccess,
  signInFailure,
  signInRequest,
  signUpSuccess,
  signUpFailure,
  signUpRequest,
  signOutSuccess,
  signOutFailure,
} from '.';

export const signIn = credentials => (dispatch, getState, { getFirebase }) => {
  const firebase = getFirebase();

  dispatch(signInRequest());

  return firebase.auth().signInWithEmailAndPassword(
    credentials.email,
    credentials.password,
  ).then(() => {
    console.log('Пользователь успешно аутентифицирован');
    dispatch(signInSuccess());
  }).catch((err) => {
    console.log('аутентификация завершилась неудачей');
    dispatch(signInFailure());
    throw err;
  });
};

export const signOut = () => (dispatch, getState, { getFirebase }) => {
  const firebase = getFirebase();

  firebase.auth().signOut()
    .then(() => {
      dispatch(signOutSuccess());
    }).catch(() => {
      dispatch(signOutFailure());
    });
};

export const signUp = newUser => (dispatch, getState, { getFirebase, getFirestore }) => {
  const firebase = getFirebase();
  const firestore = getFirestore();

  dispatch(signUpRequest());

  return firebase.auth().createUserWithEmailAndPassword(
    newUser.email,
    newUser.password,
  ).then(resp => firestore.collection('users').doc(resp.user.uid).set({
    firstName: newUser.firstName,
    lastName: newUser.lastName,
    initials: newUser.firstName[0] + newUser.lastName[0],
    products: [],
    fridge: [],
    observes: [],
  })).then(() => {
    console.log('Пользователь зарегистрирован');
    dispatch(signUpSuccess());
  })
    .catch((err) => {
      console.log('ошибка регистрации пользователя');
      dispatch(signUpFailure());
      throw err;
    });
};
