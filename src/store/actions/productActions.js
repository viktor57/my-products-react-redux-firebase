import {
  addProductSuccess,
  addProductFailure,
  addProductRequest,
  removeProductSuccess,
  removeProductFailure,
  removeProductRequest,
} from '.';

export const addProduct = product => (dispatch, getState, { getFirestore }) => {
  const firestore = getFirestore();
  const { profile } = getState().firebase;
  const userId = getState().firebase.auth.uid;

  dispatch(addProductRequest());

  return firestore.collection('users').doc(userId)
    .set({
      products: [
        ...profile.products,
        { ...product, id: new Date().getTime() },
      ],
    }, { merge: true })
    .then(() => {
      console.log('продукт добавлен');
      dispatch(addProductSuccess());
    })
    .catch((err) => {
      console.log('ошибка добавления продукта');
      dispatch(addProductFailure());
      throw err;
    });
};

export const removeProduct = productId => (dispatch, getState, { getFirestore }) => {
  const firestore = getFirestore();
  const { profile } = getState().firebase;
  const userId = getState().firebase.auth.uid;

  dispatch(removeProductRequest());

  return firestore.collection('users').doc(userId)
    .set({
      products: [
        ...profile.products.filter(i => !productId.includes(i.id)),
      ],
    }, { merge: true })
    .then(() => {
      console.log('продукты удалены');
      dispatch(removeProductSuccess());
    })
    .catch((err) => {
      console.log('ошибка удаления продукта');
      dispatch(removeProductFailure());
      throw err;
    });
};
