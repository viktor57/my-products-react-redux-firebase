import { createAction } from 'redux-actions';

export const addProductSuccess = createAction('ADD_PRODUCT_SUCCESS');
export const addProductFailure = createAction('ADD_PRODUCT_FAILURE');
export const addProductRequest = createAction('ADD_PRODUCT_REQUEST');

export const removeProductSuccess = createAction('REMOVE_PRODUCT_SUCCESS');
export const removeProductFailure = createAction('REMOVE_PRODUCT_FAILURE');
export const removeProductRequest = createAction('REMOVE_PRODUCT_REQUEST');

export const signInSuccess = createAction('SIGN_IN_SUCCESS');
export const signInFailure = createAction('SIGN_IN_FAILURE');
export const signInRequest = createAction('SIGN_IN_REQUEST');

export const signOutSuccess = createAction('SIGN_OUT_SUCCESS');
export const signOutFailure = createAction('SIGN_OUT_FAILURE');

export const signUpSuccess = createAction('SIGN_UP_SUCCESS');
export const signUpFailure = createAction('SIGN_UP_FAILURE');
export const signUpRequest = createAction('SIGN_UP_REQUEST');


export const addProductToFridgeSuccess = createAction('ADD_PRODUCT_TO_FRIDGE_SUCCESS');
export const addProductToFridgeFailure = createAction('ADD_PRODUCT_TO_FRIDGE_FAILURE');
export const addProductToFridgeRequest = createAction('ADD_PRODUCT_TO_FRIDGE_REQUEST');

export const removeProductFromFridgeSuccess = createAction('REMOVE_PRODUCT_FROM_FRIDGE_SUCCESS');
export const removeProductFromFridgeFailure = createAction('REMOVE_PRODUCT_FROM_FRIDGE_FAILURE');
export const removeProductFromFridgeRequest = createAction('REMOVE_PRODUCT_FROM_FRIDGE_REQUEST');

export const addProductToObservesSuccess = createAction('ADD_TO_OBSERVES_SUCCESS');
export const addProductToObservesFailure = createAction('ADD_TO_OBSERVES_FILURE');
export const addProductToObservesRequest = createAction('ADD_TO_OBSERVES_REQUEST');

export const removeProductFromObservesSuccess = createAction('REMOVE_FROM_OBSERVES_SUCCESS');
export const removeProductFromObservesFailure = createAction('REMOVE_FROM_OBSERVES_FILURE');
export const removeProductFromObservesRequest = createAction('REMOVE_FROM_OBSERVES_REQUEST');
