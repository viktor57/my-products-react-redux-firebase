import {
  addProductToFridgeSuccess,
  addProductToFridgeFailure,
  addProductToFridgeRequest,
  removeProductFromFridgeSuccess,
  removeProductFromFridgeFailure,
  removeProductFromFridgeRequest,
} from '.';

export const addProductToFridge = product => (dispatch, getState, { getFirestore }) => {
  const firestore = getFirestore();
  const { profile } = getState().firebase;
  const userId = getState().firebase.auth.uid;

  dispatch(addProductToFridgeRequest());

  return firestore.collection('users').doc(userId)
    .set({ fridge: [...profile.fridge, product] }, { merge: true })
    .then(() => {
      console.log('продукт добавлен');
      dispatch(addProductToFridgeSuccess());
    })
    .catch((err) => {
      console.log('ошибка добавления продукта');
      dispatch(addProductToFridgeFailure());
      throw err;
    });
};

export const removeProductFromFridge = productId => (dispatch, getState, { getFirestore }) => {
  const firestore = getFirestore();
  const { profile } = getState().firebase;
  const userId = getState().firebase.auth.uid;

  dispatch(removeProductFromFridgeRequest());

  return firestore.collection('users').doc(userId)
    .set({
      fridge: [
        ...profile.fridge.filter(i => !productId.includes(i.id)),
      ],
    }, { merge: true })
    .then(() => {
      console.log('продукты удалены');
      dispatch(removeProductFromFridgeSuccess());
    })
    .catch((err) => {
      console.log('ошибка удаления продукта');
      dispatch(removeProductFromFridgeFailure());
      throw err;
    });
};
