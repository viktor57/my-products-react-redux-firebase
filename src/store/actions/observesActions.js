import {
  addProductToObservesSuccess,
  addProductToObservesFailure,
  addProductToObservesRequest,
  removeProductFromObservesSuccess,
  removeProductFromObservesFailure,
  removeProductFromObservesRequest,
} from '.';

export const addProductToObserves = product => (dispatch, getState, { getFirestore }) => {
  const firestore = getFirestore();
  const { profile } = getState().firebase;
  const userId = getState().firebase.auth.uid;

  if ((profile.observes.find(i => product.product === i.product
      && product.categories === i.categories))) {
    console.log('продукт уже добавлен');
    return Promise.reject();
  }

  dispatch(addProductToObservesRequest());

  return firestore.collection('users').doc(userId)
    .set({
      observes: [
        ...profile.observes, product,
      ],
    }, { merge: true })
    .then(() => {
      console.log('продукт добавлен в отслеживаемые');
      dispatch(addProductToObservesSuccess());
    })
    .catch((err) => {
      console.log('ошибка добавления продукта в отслеживаемые');
      dispatch(addProductToObservesFailure());
      throw err;
    });
};

export const removeProductFromObserves = product => (dispatch, getState, { getFirestore }) => {
  const firestore = getFirestore();
  const { profile } = getState().firebase;
  const userId = getState().firebase.auth.uid;

  dispatch(removeProductFromObservesRequest());

  return firestore.collection('users').doc(userId)
    .set({
      observes: [
        ...profile.observes.filter(i => (
          (product.categories !== i.categories)
          || (product.product !== i.product)
        )),
      ],
    }, { merge: true })
    .then(() => {
      console.log('продукт удален из отслеживаемых');
      dispatch(removeProductFromObservesSuccess());
    })
    .catch((err) => {
      console.log('ошибка удаления продукта из отслеживаемых');
      dispatch(removeProductFromObservesFailure());
      throw err;
    });
};
