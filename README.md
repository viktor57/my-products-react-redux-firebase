# My Products - project for TFS by Viktor Kugay

![react logo](./src/assets/images/logo1.png)

*Приложение для менеджмента продуктов в холодильнике и продуктовой корзине*

## Использованные технологии
  - React
  - React-router
  - Redux
  - Firebase for React/Redux
  - Webpack
  - Babel
  - Material-ui

## Пример работы приложения
[my-products.surge.sh](http://my-products.surge.sh/)
