const functions = require('firebase-functions');
const admin = require('firebase-admin');
var serviceAccount = require("./key.json");
var _ = require('lodash');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://my-products-4c808.firebaseio.com"
});
